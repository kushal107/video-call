import { Route, Routes } from "react-router-dom";
import Home from "./page/home/index";
import Broadcast from "./page/video-broadcast/index"

function App() {
    return (
      <div className="App">
        <Routes>
            <Route path="/" exact element={<Home/>} />
            <Route path="/broadcast/:view" exact element={<Broadcast/>} />
        </Routes>
      </div>
    );
}

export default App;
