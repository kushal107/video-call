const broadcast = require('./video-broadcast');

function socketMain(io) {
    broadcast(io);
}
module.exports = socketMain;
